#ifndef FRACTALHELPWINDOW_H
#define FRACTALHELPWINDOW_H

#include <QMainWindow>
#include "../MainWindow/mainwindow.h"
#define FRACTAL_HELP_PATH "../ui_guides/fractal_guide.png"
#define FRACTAL_THEORY_PATH "https://www.youtube.com/watch?v=WFtTdf3I6Ug"

namespace Ui {
class FractalHelpWindow;
}

class FractalHelpWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit FractalHelpWindow(QWidget *parent = nullptr);
    void setPreviousPage(int);
    ~FractalHelpWindow();

private slots:
    void on_pushButton_back_clicked();

    void on_pushButton_uiGuide_clicked();

    void on_pushButton_theory_clicked();

private:
    Ui::FractalHelpWindow *ui;
    int previousPage;
};

#endif // FRACTALHELPWINDOW_H
