#include "helpwindow.h"
#include "ui_helpwindow.h"

HelpWindow::HelpWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::HelpWindow)
{
    ui->setupUi(this);
    this->setWindowTitle(QCoreApplication::applicationName());
    this->setFixedSize(this->width(),this->height());
}

HelpWindow::~HelpWindow()
{
    delete ui;
}

void HelpWindow::on_pushButton_back_clicked()
{
    MainWindow *mainWindow = new MainWindow;
    mainWindow->show();
    close();
}

void HelpWindow::on_pushButton_fractalHelp_clicked()
{
    FractalHelpWindow *fractalHelpWindow = new FractalHelpWindow();
    fractalHelpWindow->setPreviousPage(0);
    fractalHelpWindow->show();
    close();
}

void HelpWindow::on_pushButton_colorModelHelp_clicked()
{
    ColorHelpWindow *colorHelpWindow = new ColorHelpWindow();
    colorHelpWindow->setPreviousPage(0);
    colorHelpWindow->show();
    close();
}

void HelpWindow::on_pushButton_triangleMoveHelp_clicked()
{
    TriangleHelpWindow *triangleHelpWindow = new TriangleHelpWindow();
    triangleHelpWindow->setPreviousPage(0);
    triangleHelpWindow->show();
    close();
}
