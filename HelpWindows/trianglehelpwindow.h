#ifndef TRIANGLEHELPWINDOW_H
#define TRIANGLEHELPWINDOW_H

#include <QMainWindow>
#include "mainwindow.h"
#define TRIANGLE_HELP_PATH "../ui_guides/triangle_guide.png"
#define TRIANGLE_THEORY_PATH "https://www.youtube.com/watch?v=4I2S5Xhf24o"

namespace Ui {
class TriangleHelpWindow;
}

class TriangleHelpWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit TriangleHelpWindow(QWidget *parent = nullptr);
    void setPreviousPage(int);
    ~TriangleHelpWindow();

private slots:
    void on_pushButton_back_clicked();

    void on_pushButton_uiGuide_clicked();

    void on_pushButton_theory_clicked();

private:
    Ui::TriangleHelpWindow *ui;
    int previousPage;
};

#endif // TRIANGLEHELPWINDOW_H
