#include "fractalhelpwindow.h"
#include "ui_fractalhelpwindow.h"

FractalHelpWindow::FractalHelpWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FractalHelpWindow)
{
    ui->setupUi(this);
    this->setWindowTitle(QCoreApplication::applicationName());
    this->setFixedSize(this->width(),this->height());
}

FractalHelpWindow::~FractalHelpWindow()
{
    delete ui;
}

void FractalHelpWindow::setPreviousPage(int previousPage)
{
    this->previousPage = previousPage;

    if(previousPage == 0)
        ui->label_inactiveMiddlePath->setText(">      Help window");
    else
        ui->label_inactiveMiddlePath->setText(">  Fractal builder");
}

void FractalHelpWindow::on_pushButton_back_clicked()
{
    if(previousPage == 0)
    {
        HelpWindow *helpWindow = new HelpWindow;
        helpWindow->show();
        close();
    }
    else
    {
        FractalsBuilderWindow *fractalsBuilder = new FractalsBuilderWindow;
        fractalsBuilder->show();
        close();
    }
}

void FractalHelpWindow::on_pushButton_uiGuide_clicked()
{
    QDesktopServices::openUrl(QUrl((QFileInfo(FRACTAL_HELP_PATH).absoluteFilePath())));
}

void FractalHelpWindow::on_pushButton_theory_clicked()
{
    QDesktopServices::openUrl(QUrl(FRACTAL_THEORY_PATH, QUrl::TolerantMode));
}
