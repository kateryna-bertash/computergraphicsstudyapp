#include "trianglehelpwindow.h"
#include "ui_trianglehelpwindow.h"

TriangleHelpWindow::TriangleHelpWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TriangleHelpWindow)
{
    ui->setupUi(this);
    this->setWindowTitle(QCoreApplication::applicationName());
    this->setFixedSize(this->width(),this->height());
}

TriangleHelpWindow::~TriangleHelpWindow()
{
    delete ui;
}

void TriangleHelpWindow::setPreviousPage(int previousPage)
{
    this->previousPage = previousPage;

    if(previousPage == 0)
        ui->label_inactiveMiddlePath->setText(">           Help window");
    else
        ui->label_inactiveMiddlePath->setText(">  Triangle move editor");
}

void TriangleHelpWindow::on_pushButton_back_clicked()
{
    if(previousPage == 0)
    {
        HelpWindow *helpWindow = new HelpWindow;
        helpWindow->show();
        close();
    }
    else
    {
        TriangleWindow *triangleWindow = new TriangleWindow;
        triangleWindow->show();
        close();
    }
}

void TriangleHelpWindow::on_pushButton_uiGuide_clicked()
{
    QDesktopServices::openUrl(QUrl((QFileInfo(TRIANGLE_HELP_PATH).absoluteFilePath())));
}

void TriangleHelpWindow::on_pushButton_theory_clicked()
{
    QDesktopServices::openUrl(QUrl(TRIANGLE_THEORY_PATH, QUrl::TolerantMode));
}
