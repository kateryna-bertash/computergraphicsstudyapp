#ifndef HELPWINDOW_H
#define HELPWINDOW_H

#include <QMainWindow>
#include "mainwindow.h"
#include "fractalhelpwindow.h"
#include "colorhelpwindow.h"
#include "trianglehelpwindow.h"

namespace Ui {
class HelpWindow;
}

class HelpWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit HelpWindow(QWidget *parent = nullptr);
    ~HelpWindow();

private slots:
    void on_pushButton_back_clicked();

    void on_pushButton_fractalHelp_clicked();

    void on_pushButton_colorModelHelp_clicked();

    void on_pushButton_triangleMoveHelp_clicked();

private:
    Ui::HelpWindow *ui;
};

#endif // HELPWINDOW_H
