#include "colorhelpwindow.h"
#include "ui_colorhelpwindow.h"

ColorHelpWindow::ColorHelpWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ColorHelpWindow)
{
    ui->setupUi(this);
    this->setWindowTitle(QCoreApplication::applicationName());
    this->setFixedSize(this->width(),this->height());
}

ColorHelpWindow::~ColorHelpWindow()
{
    delete ui;
}

void ColorHelpWindow::setPreviousPage(int previousPage)
{
    this->previousPage = previousPage;
    if(previousPage == 0)
        ui->label_inactiveMiddlePath->setText(">        Help window");
    else
        ui->label_inactiveMiddlePath->setText(">  Color model editor");
}

void ColorHelpWindow::on_pushButton_back_clicked()
{
    if(previousPage == 0)
    {
        HelpWindow *helpWindow = new HelpWindow;
        helpWindow->show();
        close();
    }
    else
    {
        ColorModelWindow *colorModelWindow = new ColorModelWindow;
        colorModelWindow->show();
        close();
    }
}

void ColorHelpWindow::on_pushButton_uiGuide_clicked()
{
    QDesktopServices::openUrl(QUrl(QFileInfo(COLOR_HELP_PATH).absoluteFilePath()));
}

void ColorHelpWindow::on_pushButton_theory_clicked()
{
    QDesktopServices::openUrl(QUrl(COLOR_THEORY_PATH, QUrl::TolerantMode));
}
