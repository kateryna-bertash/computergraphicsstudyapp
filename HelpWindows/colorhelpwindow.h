#ifndef COLORHELPWINDOW_H
#define COLORHELPWINDOW_H

#include <QMainWindow>
#include "../MainWindow/mainwindow.h"
#define COLOR_HELP_PATH "../ui_guides/colors_guide.png"
#define COLOR_THEORY_PATH "https://www.youtube.com/watch?v=GyVMoejbGFg"

namespace Ui {
class ColorHelpWindow;
}

class ColorHelpWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ColorHelpWindow(QWidget *parent = nullptr);
    void setPreviousPage(int);
    ~ColorHelpWindow();

private slots:
    void on_pushButton_back_clicked();

    void on_pushButton_uiGuide_clicked();

    void on_pushButton_theory_clicked();

private:
    Ui::ColorHelpWindow *ui;
    int previousPage;
};

#endif // COLORHELPWINDOW_H
