#include "trianglewindow.h"
#include "ui_trianglewindow.h"

#include <QGraphicsItem>
#include <QGraphicsTextItem>
#include <QMatrix3x3>
#include <QTimer>

#define COORDINATE_X_0 (ui->graphicsView->width() / 2)
#define COORDINATE_Y_0 (ui->graphicsView->height() / 2)

TriangleWindow::TriangleWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TriangleWindow),
    line(NULL),
    triangle(NULL),
    _timer(NULL)
{
    ui->setupUi(this);
    this->setWindowTitle(QCoreApplication::applicationName());
    this->initToolTips();
    this->setFixedSize(this->width(),this->height());

    // Set scene
    scene = new QGraphicsScene();
    scene->setSceneRect(0,0,ui->graphicsView->width(),ui->graphicsView->height());
    ui->graphicsView->setScene(scene);

    // Label 0 coordinate
    QGraphicsItem *s = new QGraphicsSimpleTextItem("0");
    s->setPos(QPointF(ui->graphicsView->width() / 2,ui->graphicsView->height() / 2));
    scene->addItem(s);

    for (int  i = 0 ; i < 3 ; ++i)
        pointLables[i] = NULL;

    // Coordinate lines
    drawCoordinates();

    // Build Line
    buildLine();

    // Build Triangle
    buildTriangle();

}

TriangleWindow::~TriangleWindow()
{
    delete triangle;
    delete line;
    delete scene;
    delete ui;
}

void TriangleWindow::drawCoordinates()
{
    QPointF upper(ui->graphicsView->width() / 2, 0);
    QPointF lower(ui->graphicsView->width() / 2, ui->graphicsView->height());
    QPointF left(0,  ui->graphicsView->height() / 2);
    QPointF right(ui->graphicsView->width(), ui->graphicsView->height() / 2);
    scene->addLine(QLineF(upper, lower));
    scene->addLine(QLineF(right, left));


    // Add labels for coordinates TODO

//    for (int i = 0; i < ui->graphicsView->width() - 200; i += 200)
//    {
//        QGraphicsItem *s = new QGraphicsSimpleTextItem(QString::number(i));
//        s->setPos(i + 10,ui->graphicsView->height() / 2 - 10);
//        scene->addItem(s);
//    }
//    for (int i = 0; i < ui->graphicsView->height() - 200; i += 200)
//    {
//        QGraphicsItem *s = new QGraphicsSimpleTextItem(QString::number(i));
//        s->setPos(ui->graphicsView->width() / 2 - 10, i + 10);
//        scene->addItem(s);
//    }

}

void TriangleWindow::buildLine()
{
    if (line) scene->removeItem((QGraphicsItem *)line);
    double A = ui->lineEdit_lineCoefA->text().toDouble();
    double B = ui->lineEdit_lineCoefB->text().toDouble();
    double C = ui->lineEdit_lineCoefC->text().toDouble();


    if (A == 0) line = scene->addLine(QLineF(QPointF(0, C / B - ui->graphicsView->height() / 2), QPointF(ui->graphicsView->width(), C / B - ui->graphicsView->height() / 2)));
    else if (B == 0) line = scene->addLine(QLineF(QPointF(- C / A - ui->graphicsView->width() / 2,0), QPointF(- C / A - ui->graphicsView->width(),ui->graphicsView->height())));
    else
    {
        QPointF first(0, - A * ui->graphicsView->width() / 2 / B + ui->graphicsView->height() / 2 + C / B);
        QPointF second( ui->graphicsView->width() / 2 - B * ui->graphicsView->height() / 2 / A - C / A , 0);
//        qDebug() << first.y() << " " << second.x() << "\n";
        line = scene->addLine(QLineF(first, second));
    }
}

void TriangleWindow::buildTriangle()
{
    // Clear scene
    if (triangle) scene->removeItem(triangle);
    for (int  i = 0 ; i < 3 ; ++i)
        if (pointLables[i]) scene->removeItem(pointLables[i]);

    //Set points
    QPointF A(ui->graphicsView->width() / 2 + ui->lineEdit_x1->text().toDouble(), ui->graphicsView->height() / 2 - ui->lineEdit_y1->text().toDouble());
    QPointF B(ui->graphicsView->width() / 2 + ui->lineEdit_x2->text().toDouble(), ui->graphicsView->height() / 2 - ui->lineEdit_y2->text().toDouble());

    double lineSize = sqrt(pow(A.x() - B.x(),2) + pow(A.y() - B.y(),2));
    double cosAngle = (A.x() - B.x()) / lineSize;
    double sinAngle = (A.y() - B.y()) / lineSize;
    double height = ui->lineEdit_heightLen->text().toDouble();

    int direction = ui->comboBox_heightAngle->currentIndex() == 0 ? -1 : 1;

    QPointF C;
    C.setX( direction * height * sinAngle + (A.x() + B.x()) / 2);
    C.setY( - direction * height * cosAngle + (A.y() + B.y()) / 2);

    qDebug() << lineSize << " " << cosAngle << " " << sinAngle << " " << height << " " << A.x() << " " << A.y() <<" " << B.x() <<" " << B.y() <<" " << C.x() <<" " << C.y() << "\n";
    triangle = scene->addPolygon(QPolygonF({A,B,C}));

    //Set labels
    pointLables[0] = scene->addSimpleText("A (" + QString::number(A.x() - ui->graphicsView->width() / 2) + " ; " + QString::number(ui->graphicsView->height() / 2 - A.y()) + ")");
    pointLables[0]->setPos(A);
    pointLables[1] = scene->addSimpleText("B (" + QString::number(B.x() - ui->graphicsView->width() / 2) + " ; " + QString::number(ui->graphicsView->height() / 2 - B.y()) + ")");
    pointLables[1]->setPos(B);
    pointLables[2] = scene->addSimpleText("C (" + QString::number(C.x() - ui->graphicsView->width() / 2) + " ; " + QString::number(ui->graphicsView->height() / 2 - C.y()) + ")");
    pointLables[2]->setPos(C);
}

void TriangleWindow::on_pushButton_back_clicked()
{
    if(changesNotSavedDialog() == QMessageBox::Cancel)
        return;

    MainWindow *mainWindow = new MainWindow;
    mainWindow->show();
    close();
}

void TriangleWindow::on_pushButton_helpWhite_clicked()
{
    if(changesNotSavedDialog() == QMessageBox::Cancel)
        return;

    TriangleHelpWindow *triangleHelpWindow = new TriangleHelpWindow;
    triangleHelpWindow->setPreviousPage(1);
    triangleHelpWindow->show();
    close();
}

void TriangleWindow::on_lineEdit_lineCoefA_editingFinished()
{
    bool ok;
    float A = ui->lineEdit_lineCoefA->text().toFloat(&ok);
    float B = ui->lineEdit_lineCoefB->text().toFloat();

    if (!ui->lineEdit_lineCoefA->text().isEmpty() && ok &&
            !(A == B && A + B == 0))
        this->buildLine();
    else
        ui->lineEdit_lineCoefA->setText(""); // Input is not number
}

void TriangleWindow::on_lineEdit_lineCoefB_editingFinished()
{
    bool ok;
    float B = ui->lineEdit_lineCoefB->text().toFloat(&ok);
    float A = ui->lineEdit_lineCoefA->text().toFloat();

    if (!ui->lineEdit_lineCoefB->text().isEmpty() && ok &&
            !(A == B && A + B == 0))
        this->buildLine();
    else
        ui->lineEdit_lineCoefB->setText(""); // Input is not number
}


void TriangleWindow::on_lineEdit_lineCoefC_editingFinished()
{
    bool ok;
    ui->lineEdit_lineCoefC->text().toFloat(&ok);
    if (!ui->lineEdit_lineCoefC->text().isEmpty() && ok)
        this->buildLine();
    else
        ui->lineEdit_lineCoefC->setText(""); // Input is not number
}


void TriangleWindow::on_lineEdit_x1_editingFinished()
{
    bool ok;
    float x1 = ui->lineEdit_x1->text().toFloat(&ok);
    float x2 = ui->lineEdit_x2->text().toFloat();
    float y1 = ui->lineEdit_y1->text().toFloat();
    float y2 = ui->lineEdit_y2->text().toFloat();
    if (!ui->lineEdit_x1->text().isEmpty() && ok &&
            !(x1 == x2 && y1 == y2))
        this->buildTriangle();
    else
        ui->lineEdit_x1->setText(""); // Input is not number
}


void TriangleWindow::on_lineEdit_y1_editingFinished()
{
    bool ok;
    float x1 = ui->lineEdit_x1->text().toFloat();
    float x2 = ui->lineEdit_x2->text().toFloat();
    float y1 = ui->lineEdit_y1->text().toFloat(&ok);
    float y2 = ui->lineEdit_y2->text().toFloat();
    if (!ui->lineEdit_y1->text().isEmpty() && ok &&
            !(x1 == x2 && y1 == y2))
        this->buildTriangle();
    else
        ui->lineEdit_y1->setText(""); // Input is not number
}


void TriangleWindow::on_lineEdit_x2_editingFinished()
{
    bool ok;
    float x1 = ui->lineEdit_x1->text().toFloat();
    float x2 = ui->lineEdit_x2->text().toFloat(&ok);
    float y1 = ui->lineEdit_y1->text().toFloat();
    float y2 = ui->lineEdit_y2->text().toFloat();
    if (!ui->lineEdit_x2->text().isEmpty() && ok &&
            !(x1 == x2 && y1 == y2))
        this->buildTriangle();
    else
        ui->lineEdit_x2->setText(""); // Input is not number
}


void TriangleWindow::on_lineEdit_y2_editingFinished()
{
    bool ok;
    float x1 = ui->lineEdit_x1->text().toFloat();
    float x2 = ui->lineEdit_x2->text().toFloat();
    float y1 = ui->lineEdit_y1->text().toFloat();
    float y2 = ui->lineEdit_y2->text().toFloat(&ok);
    if (!ui->lineEdit_y2->text().isEmpty() && ok &&
            !(x1 == x2 && y1 == y2))
        this->buildTriangle();
    else
        ui->lineEdit_y2->setText(""); // Input is not number
}


void TriangleWindow::on_comboBox_heightAngle_currentIndexChanged(int index)
{
    this->buildTriangle();
}


void TriangleWindow::on_lineEdit_heightLen_editingFinished()
{
    bool ok;
    float height = ui->lineEdit_heightLen->text().toFloat(&ok);
    if (!ui->lineEdit_heightLen->text().isEmpty() && ok && height > 0)
        this->buildTriangle();
    else
        ui->lineEdit_heightLen->setText(""); // Input is not number
}


void TriangleWindow::on_pushButton_move_clicked()
{
    if (!_timer)
    {
        _timer = new QTimer(this);
        connect(_timer, &QTimer::timeout, this, &TriangleWindow::onTimeout);
        _timer->start(500);
    }
}


void TriangleWindow::on_pushButton_stop_clicked()
{
    if (_timer)
    {
        _timer->stop();
        delete _timer;
        _timer = NULL;
    }

    buildTriangle();
}


void TriangleWindow::onTimeout()
{
    double A = ui->lineEdit_lineCoefA->text().toDouble();
    double B = ui->lineEdit_lineCoefB->text().toDouble();
    double C = ui->lineEdit_lineCoefC->text().toDouble();

    QMatrix3x3 oldTriangleMatrix;
    float *tr_mat = oldTriangleMatrix.data();
    tr_mat[0] = triangle->polygon()[0].x() - ui->graphicsView->width() / 2;
    tr_mat[3] = -triangle->polygon()[0].y() + ui->graphicsView->height() / 2;
    tr_mat[1] = triangle->polygon()[1].x() - ui->graphicsView->width() / 2;
    tr_mat[4] = -triangle->polygon()[1].y() + ui->graphicsView->height() / 2;
    tr_mat[2] = triangle->polygon()[2].x() - ui->graphicsView->width() / 2;
    tr_mat[5] = -triangle->polygon()[2].y() + ui->graphicsView->height() / 2;
    tr_mat[6] = tr_mat[7] = tr_mat[8] = 1;

    qDebug() << oldTriangleMatrix;

    // Move
    QMatrix3x3 moveMatrix;
    moveMatrix.setToIdentity();
    float *m_mat = moveMatrix.data();
    m_mat[5] = - C / B;

    QMatrix3x3 moveMatrix2;
    moveMatrix2.setToIdentity();
    float *m_mat2 = moveMatrix2.data();
    m_mat2[5] = C / B;

    qDebug() << moveMatrix;

    // Rotate
    QMatrix3x3 rotateMatrix;
    float *rot_mat = rotateMatrix.data();
    rot_mat[0] = cos(-atan(-A/B));
    rot_mat[1] = sin(-atan(-A/B));
    rot_mat[3] = -sin(-atan(-A/B));
    rot_mat[4] = cos(-atan(-A/B));
    rot_mat[2] = rot_mat[5] = rot_mat[6] = rot_mat[7] = 0;
    rot_mat[8] = 1;

    QMatrix3x3 rotateMatrix2 = rotateMatrix;
    float *rot_mat2 = rotateMatrix2.data();
    rot_mat2[0] = cos(atan(-A/B));
    rot_mat2[1] = sin(atan(-A/B));
    rot_mat2[3] = -sin(atan(-A/B));
    rot_mat2[4] = cos(atan(-A/B));
    rot_mat2[2] = rot_mat2[5] = rot_mat2[6] = rot_mat2[7] = 0;
    rot_mat2[8] = 1;

    qDebug() << rotateMatrix;
    qDebug() << rotateMatrix2;

    // Reflect
    QMatrix3x3 reflectMatrix;
    reflectMatrix.setToIdentity();
    float *ref_mat = reflectMatrix.data();
    ref_mat[4] = -1;

    qDebug() << reflectMatrix;

    QMatrix3x3 changingMatrix = moveMatrix2 * rotateMatrix2 * reflectMatrix * rotateMatrix * moveMatrix;

    qDebug() << changingMatrix;

    QMatrix3x3 newTriangleMatrix = oldTriangleMatrix * changingMatrix;

    qDebug() << newTriangleMatrix;

    // Clear scene
    if (triangle) scene->removeItem(triangle);
    for (int  i = 0 ; i < 3 ; ++i)
        if (pointLables[i]) scene->removeItem(pointLables[i]);
    QPointF Ap(newTriangleMatrix.data()[0] + ui->graphicsView->width() / 2,-newTriangleMatrix.data()[3] + ui->graphicsView->height() / 2);
    QPointF Bp(newTriangleMatrix.data()[1] + ui->graphicsView->width() / 2,-newTriangleMatrix.data()[4] + ui->graphicsView->height() / 2);
    QPointF Cp(newTriangleMatrix.data()[2] + ui->graphicsView->width() / 2,-newTriangleMatrix.data()[5] + ui->graphicsView->height() / 2);

    triangle = scene->addPolygon(QPolygonF({Ap,Bp,Cp}));
    qDebug() << Ap.x() << " " << Ap.y() <<" " << Bp.x() <<" " << Bp.y() <<" " << Cp.x() <<" " << Cp.y() << "\n";

    //Set labels
    pointLables[0] = scene->addSimpleText("A (" + QString::number(Ap.x() - ui->graphicsView->width() / 2) + " ; " + QString::number(ui->graphicsView->height() / 2 - Ap.y()) + ")");
    pointLables[0]->setPos(Ap);
    pointLables[1] = scene->addSimpleText("B (" + QString::number(Bp.x() - ui->graphicsView->width() / 2) + " ; " + QString::number(ui->graphicsView->height() / 2 - Bp.y()) + ")");
    pointLables[1]->setPos(Bp);
    pointLables[2] = scene->addSimpleText("C (" + QString::number(Cp.x() - ui->graphicsView->width() / 2) + " ; " + QString::number(ui->graphicsView->height() / 2 - Cp.y()) + ")");
    pointLables[2]->setPos(Cp);

}

QMessageBox::StandardButton TriangleWindow::changesNotSavedDialog()
{
    QMessageBox::StandardButton reply = QMessageBox::warning(this,
                                                             "Warning",
                                                             "Do you really want to exit this window?\n"
                                                             "Changes are not saved",
                                                             QMessageBox::Save |
                                                             QMessageBox::Close|
                                                             QMessageBox::Cancel);

    if(reply == QMessageBox::Save)
        on_pushButton_save_clicked();

    return reply;
}

void TriangleWindow::initToolTips()
{
    ui->pushButton_helpWhite->setToolTip("Go triangle move help window");

    ui->pushButton_back->setToolTip("Go to the previous window");

    ui->pushButton_save->setToolTip("Save builded affine transformation to your pc in png format");

    ui->label_activePath->setToolTip("The current directory");

    ui->pushButton_move->setToolTip("Start the process of moving the triangle");

    ui->pushButton_stop->setToolTip("Stop moving the triangle");

    ui->label_lineEquation->setToolTip("Line equation to build a line according to which we "
                                       "perform affine transformations of the triangle");

    ui->lineEdit_lineCoefA->setToolTip("The coefficient near X");

    ui->lineEdit_lineCoefB->setToolTip("The coefficient near Y");

    ui->lineEdit_lineCoefC->setToolTip("The free coefficient");

    ui->lineEdit_heightLen->setToolTip("The length of the triangle's height."
                                    "\nWe need that to set a ration of triangle's particles");

    ui->comboBox_heightAngle->setToolTip("The angle of the triangle's height");

    ui->label_vertex1->setToolTip("We need to set the coordinates of the first basic vertext to build a triangle");

    ui->label_vertex2->setToolTip("We need to set the coordinates of the second basic vertext to build a triangle");
}

void TriangleWindow::on_pushButton_save_clicked()
{
    QString fileName= QFileDialog::getSaveFileName(this,
                                                   "Save image",
                                                   QCoreApplication::applicationDirPath(),
                                                   "PNG (*.png)" );

    if (!fileName.isNull())
    {
        //Copy qgraphicsView to not change scaling of the original graphicsView
        QGraphicsView *graphicsView = new QGraphicsView(ui->graphicsView->scene());
        graphicsView->setMinimumSize(ui->graphicsView->width(), ui->graphicsView->height());
        graphicsView->fitInView(graphicsView->scene()->sceneRect(), Qt::KeepAspectRatio );

        QImage image(graphicsView->scene()->sceneRect().size().toSize(), QImage::Format_ARGB32);
        image.fill(Qt::transparent);

        QPainter painter(&image);
        graphicsView->scene()->render(&painter);
        image.save(fileName);
    }
}
