#ifndef TRIANGLEWINDOW_H
#define TRIANGLEWINDOW_H

#include <QMainWindow>
#include "mainwindow.h"

namespace Ui {
class TriangleWindow;
}

class TriangleWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit TriangleWindow(QWidget *parent = nullptr);
    ~TriangleWindow();

    void buildLine();

    void buildTriangle();

    void drawCoordinates();

    QGraphicsScene *scene;

    QGraphicsLineItem *line;

    QGraphicsPolygonItem *triangle;

    QGraphicsSimpleTextItem *pointLables[3];

    QTimer *_timer;

    void onTimeout();

private slots:
    void on_pushButton_back_clicked();

    void on_pushButton_helpWhite_clicked();

    void on_lineEdit_lineCoefA_editingFinished();

    void on_lineEdit_lineCoefB_editingFinished();

    void on_lineEdit_lineCoefC_editingFinished();

    void on_lineEdit_x1_editingFinished();

    void on_lineEdit_y1_editingFinished();

    void on_lineEdit_x2_editingFinished();

    void on_lineEdit_y2_editingFinished();

    void on_comboBox_heightAngle_currentIndexChanged(int index);

    void on_lineEdit_heightLen_editingFinished();

    void on_pushButton_move_clicked();

    void on_pushButton_stop_clicked();

    void on_pushButton_save_clicked();

private:
    Ui::TriangleWindow *ui;

    QMessageBox::StandardButton changesNotSavedDialog();

    void initToolTips();
};

#endif // TRIANGLEWINDOW_H
