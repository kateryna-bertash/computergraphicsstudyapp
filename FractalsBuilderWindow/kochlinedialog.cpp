#include "kochlinedialog.h"
#include "ui_kochlinedialog.h"

KochLineDialog::KochLineDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::KochLineDialog)
{
    ui->setupUi(this);
    this->setWindowTitle(QCoreApplication::applicationName());
    this->setFixedSize(this->width(),this->height());
    this->initToolTips();
    scene = new QGraphicsScene();
    ui->graphicsView_kochLine->setScene(scene);
}

QGraphicsScene *KochLineDialog::getScene()
{
    return this->scene;
}
QGraphicsView *KochLineDialog::getView()
{
    return ui->graphicsView_kochLine;
}

void KochLineDialog::scaleSceneToFitView()
{
    ui->graphicsView_kochLine->fitInView(ui->graphicsView_kochLine->scene()->sceneRect(), Qt::KeepAspectRatio );
}

void KochLineDialog::wheelEvent(QWheelEvent *event){

    //Zoom in/out on mouse wheel event
    ui->graphicsView_kochLine->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);

    if(event->angleDelta().y() > 0)
    {
        ui->graphicsView_kochLine->scale(FRACTAL_SCALE_FACTOR, FRACTAL_SCALE_FACTOR);
    }
    else
    {
         ui->graphicsView_kochLine->scale(1.0 / FRACTAL_SCALE_FACTOR, 1.0 / FRACTAL_SCALE_FACTOR);
    }
}

void KochLineDialog::initToolTips()
{
    ui->label_activePath->setToolTip("The current directory");

    ui->label_needHelp->setToolTip("A Koch curve is a fractal curve that can be constructed by taking a straight "
                                   "\nline segment and replacing it with a pattern of multiple line segments."
                                   "\nThen the line segments in that pattern are replaced by the same pattern."
                                   "\nThen line segments in that are replaced,... etc., etc., etc ...  ");

    ui->pushButton_kochLineNext->setToolTip("Continue the process of building koch fractal based on the built koch line");
}

KochLineDialog::~KochLineDialog()
{
    delete ui;
}

void KochLineDialog::on_pushButton_kochLineNext_clicked()
{
    this->close();
}

