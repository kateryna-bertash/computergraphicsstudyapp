#include "fractalsbuilderwindow.h"
#include "ui_fractalsbuilderwindow.h"
#include "Fractal.h"

#define LINE_LENGTH 900

FractalsBuilderWindow::FractalsBuilderWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FractalsBuilderWindow)
{
    ui->setupUi(this);
    this->setWindowTitle(QCoreApplication::applicationName());
    this->initToolTips();
    this->setFixedSize(this->width(),this->height());
}

FractalsBuilderWindow::~FractalsBuilderWindow()
{
    delete ui;
}

void FractalsBuilderWindow::on_pushButton_back_clicked()
{
    if(changesNotSavedDialog() == QMessageBox::Cancel)
        return;

    MainWindow *mainWindow = new MainWindow;
    mainWindow->show();
    close();
}

void FractalsBuilderWindow::on_pushButton_helpWhite_clicked()
{
    if(changesNotSavedDialog() == QMessageBox::Cancel)
        return;

    FractalHelpWindow *fractalHelpWindow = new FractalHelpWindow();
    fractalHelpWindow->setPreviousPage(1);
    fractalHelpWindow->show();
    close();
}

void FractalsBuilderWindow::on_pushButton_build_clicked()
{
    int iterations = ui->spinBox_fractalIteration->value();
    int angles = !(ui->comboBox_fractalShape->currentIndex()) ? 3 : 4; // index == 0 - Triangle; else - Square
    QGraphicsScene * scene = new QGraphicsScene();
    ui->graphicsView->setScene(scene);

    if(ui->comboBox_fractalType->currentIndex() == 0) // if index = 0 - Koch fractal; else - Serpinskii
    {
        QPointF start_point(-300, 0);
        int line_size = LINE_LENGTH / (angles);
        QPoint end_point( start_point.x() + LINE_LENGTH, 0);
        float height = ui->doubleSpinBox_fractalHeight->value() * LINE_LENGTH / 6 / angles * sqrt(3);

        kochLineDialog = new KochLineDialog(); //Open the dialog
        kochLineDialog->setModal(true);
        Fractal::DrawKochCurve(kochLineDialog->getView(), kochLineDialog->getScene(), iterations, 1, start_point, end_point, height, angles);

        kochLineDialog->exec();

        Fractal::DrawKochSnowflake(ui->graphicsView, scene, angles, iterations, start_point, line_size, height);
    }
    else
    {
        Fractal::DrawSierpinskiFractal(ui->graphicsView, scene, iterations, angles);
    }
    this->scaleSceneToFitView(ui->graphicsView);
}


void FractalsBuilderWindow::on_pushButton_save_clicked()
{
    QString fileName= QFileDialog::getSaveFileName(this, "Save image", QCoreApplication::applicationDirPath(), "PNG (*.png)" );

    if (!fileName.isNull())
    {
        //Copy qgraphicsView to not change scaling of the original graphicsView
        QGraphicsView *graphicsView = new QGraphicsView(ui->graphicsView->scene());
        graphicsView->setMinimumSize(ui->graphicsView->width(), ui->graphicsView->height());
        this->scaleSceneToFitView(graphicsView);

        QImage image(graphicsView->scene()->sceneRect().size().toSize(), QImage::Format_ARGB32);
        image.fill(Qt::transparent);

        QPainter painter(&image);
        graphicsView->scene()->render(&painter);
        image.save(fileName);
    }
}

void FractalsBuilderWindow::on_comboBox_fractalType_activated(int index)
{
    if (index == 1)
    {
        ui->doubleSpinBox_fractalHeight->setEnabled(false);
    }
    else
    {
        ui->doubleSpinBox_fractalHeight->setEnabled(true);
    }
}

void FractalsBuilderWindow::wheelEvent(QWheelEvent *event){

    //Zoom in/out on mouse wheel event
    ui->graphicsView->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);

    if(event->angleDelta().y() > 0)
    {
        ui->graphicsView->scale(FRACTAL_SCALE_FACTOR, FRACTAL_SCALE_FACTOR);
    }
    else
    {
         ui->graphicsView->scale(1.0 / FRACTAL_SCALE_FACTOR, 1.0 / FRACTAL_SCALE_FACTOR);
    }
}

void FractalsBuilderWindow::scaleSceneToFitView(QGraphicsView *graphicsView)
{
    graphicsView->fitInView(graphicsView->scene()->sceneRect(), Qt::KeepAspectRatio );
}

QMessageBox::StandardButton FractalsBuilderWindow::changesNotSavedDialog()
{
    QMessageBox::StandardButton reply = QMessageBox::warning(this, "Warning",
                                                             "Do you really want to exit this window?\n"
                                                             "Changes are not saved",
                                        QMessageBox::Save|QMessageBox::Close|QMessageBox::Cancel);

    if(reply == QMessageBox::Save)
        on_pushButton_save_clicked();

    return reply;
}

void FractalsBuilderWindow::initToolTips()
{
    ui->pushButton_helpWhite->setToolTip("Go fractal help window");
    ui->pushButton_back->setToolTip("Go to the previous window");
    ui->pushButton_save->setToolTip("Save builded fractal to your pc in png format");
    ui->label_activePath->setToolTip("The current directory");
    ui->pushButton_build->setToolTip("Start the process of building the fractal with choosen parameters");

    ui->comboBox_fractalType->setToolTip("1.The Koch snowflake (also known as the Koch curve, Koch star, or Koch island)"
                                         "\nis a mathematical curve and one of the earliest fractal curves to have been described"
                                         "\n2.The Sierpiński fractal is a fractal attractive fixed set with the overall shape of an "
                                         "\nequilateral basic figure, subdivided recursively into smaller equilateral basic figures.");


    ui->comboBox_fractalShape->setToolTip("Basic figure to build koch snowflake or serpinski fractal");

    ui->doubleSpinBox_fractalHeight->setToolTip("Height value is needed to build a koch fractal:"
                                                "\nto establish the ratio of the parties of triangle");

    ui->spinBox_fractalIteration->setToolTip("We need the count of iterations to know how many times the algorithm to build fractal is apllied."
                                             "\nThe result of each iteration is used as the starting values for the next");
}
