#ifndef KOCHLINEDIALOG_H
#define KOCHLINEDIALOG_H

#include <QDialog>
#include <QGraphicsScene>
#include <QWheelEvent>
#define FRACTAL_SCALE_FACTOR 1.15

namespace Ui {
class KochLineDialog;
}

class KochLineDialog : public QDialog
{
    Q_OBJECT

public:
    explicit KochLineDialog(QWidget *parent = nullptr);
    QGraphicsScene * getScene();
    QGraphicsView * getView();
    void scaleSceneToFitView();

    ~KochLineDialog();

private slots:
    void on_pushButton_kochLineNext_clicked();

private:
    Ui::KochLineDialog *ui;

    int angles;
    int iterations;
    QGraphicsScene * scene;

    virtual void wheelEvent(QWheelEvent* event);

    void initToolTips();
};

#endif // KOCHLINEDIALOG_H
