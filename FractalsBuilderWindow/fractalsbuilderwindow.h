#ifndef FRACTALSBUILDERWINDOW_H
#define FRACTALSBUILDERWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QWheelEvent>
#include <QGraphicsView>
#include "mainwindow.h"
#include "kochlinedialog.h"
#include <cmath>
#include <QMessageBox>

#define FRACTAL_SCALE_FACTOR 1.15

namespace Ui {
class FractalsBuilderWindow;
}

class FractalsBuilderWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit FractalsBuilderWindow(QWidget *parent = nullptr);
    ~FractalsBuilderWindow();

private slots:
    void on_pushButton_back_clicked();

    void on_pushButton_helpWhite_clicked();

    void on_pushButton_build_clicked();

    void on_pushButton_save_clicked();

    void on_comboBox_fractalType_activated(int index);

private:
    Ui::FractalsBuilderWindow *ui;

    KochLineDialog *kochLineDialog;

    virtual void wheelEvent(QWheelEvent* event);

    void scaleSceneToFitView(QGraphicsView *);

    QMessageBox::StandardButton changesNotSavedDialog();

    void initToolTips();
};

#endif // FRACTALSBUILDERWINDOW_H
