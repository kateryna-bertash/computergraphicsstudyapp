#include "mainwindow.h"
#include <QFile>
#include <QApplication>
#include<QtGui>

#define STYLESHEET_FILE_PATH ":/stylesheet/resources/stylesheet/mainWindowStyles.qss"
#define QUICKSEND_FILE_PATH ":/font/resources/font/Quicksand/static/Quicksand-Medium.ttf"
#define APPLICATION_ICON_PATH ":/image/resources/image/app_icon.png"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    a.setApplicationName(APPLICATION_HEADER);
    a.setWindowIcon(QIcon(APPLICATION_ICON_PATH));
    MainWindow w;

    QFontDatabase qfontDB;
    qfontDB.addApplicationFont(QUICKSEND_FILE_PATH);

    QFile stylesheet_file(STYLESHEET_FILE_PATH);
    stylesheet_file.open(QFile::ReadOnly);
    QString stylesheet = QLatin1String(stylesheet_file.readAll());
    qApp->setStyleSheet(stylesheet);

    w.show();
    return a.exec();
}
