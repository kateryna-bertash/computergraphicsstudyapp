#ifndef KOCHSNOWFLAKE_H
#define KOCHSNOWFLAKE_H
#include <QGraphicsView>

class Fractal
{
public:
    Fractal();
    static void DrawKochSnowflake(QGraphicsView* view,
                     QGraphicsScene * scene,
                     int angles,
                     int iterations,
                     QPointF start_point,
                     float line_size,
                     float ratio);

    static void DrawKochCurve(QGraphicsView* view,
                                QGraphicsScene * scene,
                                int iterations,
                                int current_iteration,
                                QPointF start_point,
                                QPointF end_point,
                                float ratio,
                                int angles_count);

    static void DrawSierpinskiTriangle(QGraphicsView* view,
                                       QGraphicsScene * scene,
                                       int iterations,
                                       int current_iteration,
                                       QPoint p1,
                                       QPoint p2,
                                       QPoint p3);

    static void DrawSierpinskiSquare(QGraphicsView* view,
                                     QGraphicsScene * scene,
                                     int iterations,
                                     int current_iteration,
                                     QPoint p1,
                                     QPoint p2,
                                     QPoint p3,
                                     QPoint p4);

    static void DrawSierpinskiFractal(QGraphicsView* view,
                                      QGraphicsScene * scene,
                                      int iterations,
                                      int num_of_points);

};

#endif // KOCHSNOWFLAKE_H
