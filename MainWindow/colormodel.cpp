#include "colormodel.h"

std::vector<std::pair<int, int>> ColorModel::pixels;

void ColorModel::applySaturationChange(QImage &image,
                                       const int &newSaturation,
                                       const std::pair<int, int> & pixel)
{
    QRgb rgb = image.pixel(pixel.second, pixel.first);

    int h = 0, s = 0, v = 0;
    convertRgbToHsv(rgb, h, s, v);

    s += newSaturation;
    s = s >= 100 ? 100 : s;
    s = s < 0 ? 0 : s;

    image.setPixel(pixel.second, pixel.first, convertHsvToRgb(h,s,v));
}

void ColorModel::setPixels(const QImage & image)
{
    pixels.clear();
    int h = 0, s = 0, v = 0;

    //Find all magenta and green pixels in image
    for(int i = 0; i < image.height(); ++i)
    {
        for(int j = 0; j < image.width(); ++j)
        {
            QRgb rgb = image.pixel(j,i);
            convertRgbToHsv(rgb, h, s, v);

            if(h >= MAGENTA_HUE_VALUE - RANGE_SHIFT && h <= MAGENTA_HUE_VALUE + RANGE_SHIFT)
                pixels.push_back(std::make_pair(i, j));

            if(h >= GREEN_HUE_VALUE - RANGE_SHIFT && h <= GREEN_HUE_VALUE + RANGE_SHIFT)
                pixels.push_back(std::make_pair(i, j));
        }
    }
}

void ColorModel::convertRgbToHsv(const QRgb & rgb, int & h, int & s, int & v)
{
    double r = (double)qRed(rgb)   / RANGE_TRANSFORM_VALUE;
    double g = (double)qGreen(rgb) / RANGE_TRANSFORM_VALUE;
    double b = (double)qBlue(rgb)  / RANGE_TRANSFORM_VALUE;

    // h, s, v = hue, saturation, value
    double cmax = std::max(r, std::max(g, b)); // maximum of r, g, b
    double cmin = std::min(r, std::min(g, b)); // minimum of r, g, b
    double diff = cmax - cmin; // diff of cmax and cmin.

    if(cmax == cmin)
        h = 0;

    else if(cmax == r)
        h = std::fmod((60 * ((g - b) / diff) + 360), 360.0);

    // if cmax equal g then compute h
    else if (cmax == g)
        h = std::fmod((60 * ((b - r) / diff) + 120), 360.0);

    // if cmax equal b then compute h
    else if (cmax == b)
        h = std::fmod((60 * ((r - g) / diff) + 240), 360.0);

    s = cmax == 0 ? 0 : (diff / cmax) * 100;

    // compute v
    v = cmax * 100;
}

void ColorModel::convertRgbToCMYK(const QRgb &rgb, int &c, int &m, int &y, int &k)
{
    double r = (double)qRed(rgb)   / RANGE_TRANSFORM_VALUE;
    double g = (double)qGreen(rgb) / RANGE_TRANSFORM_VALUE;
    double b = (double)qBlue(rgb)  / RANGE_TRANSFORM_VALUE;

    //If the rgb is black (0, 0, 0)
    //Apllied to avoid the division by ZERO in the following formula
    if(r == 0 && g == 0 && b == 0)
    {
        c = 0;
        m = 0;
        y = 0;
        k = 100;
        return;
    }

    double kTmp = (1 - std::max(std::max(r, g), b));
    c = ((1 - r - kTmp) / (1 - kTmp))*100.0;
    m = ((1 - g - kTmp) / (1 - kTmp))*100.0;
    y = ((1 - b - kTmp) / (1 - kTmp))*100.0;
    k = kTmp * 100.0;
}

bool ColorModel::changeSaturation(QImage &image, QRect & rect, const int &saturation)
{
    if(pixels.empty())
        return false;//If there are no magenta/green color in image

    for(const auto & pixel : pixels)
    {
        if(rect.width() != 0 && rect.height() != 0) //If there is selection rectangle on the canvas
        {
            if(pixel.second >= rect.x() && pixel.second < rect.x() + rect.width()
                && pixel.first > rect.y() && pixel.first < rect.y() + rect.height())
            {
                applySaturationChange(image, saturation, pixel);
            }
        }
        else //If we need to change saturation for the full image
        {
            applySaturationChange(image, saturation, pixel);
        }
    }

    return true;
}

QRgb ColorModel::convertHsvToRgb(const int &H, const int &S, const int &V)
{
    double s = S / 100.0;
    double v = V / 100.0;
    double C = s * v;
    double X = C * (1 - std::abs(std::fmod( H / 60.0, 2) - 1));
    double m = v-C;
    double r = 0, g = 0, b = 0;

    if(H >= 0 && H < 60)
    {
        r = C, g = X, b = 0;
    }
    else if(H >= 60 && H < 120)
    {
        r = X, g = C, b = 0;
    }
    else if(H >= 120 && H < 180)
    {
        r = 0, g = C, b = X;
    }
    else if(H >= 180 && H < 240)
    {
        r = 0, g = X, b = C;
    }
    else if(H >= 240 && H < 300)
    {
        r = X, g = 0, b = C;
    }
    else
    {
        r = C, g = 0, b = X;
    }

    int R = (r + m) * RANGE_TRANSFORM_VALUE;
    int G = (g + m) * RANGE_TRANSFORM_VALUE;
    int B = (b + m) * RANGE_TRANSFORM_VALUE;

    return qRgb(R,G,B);
}
