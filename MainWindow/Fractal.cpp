#include "Fractal.h"
#include <QMatrix4x4>
#include <QVector2D>
#include <cmath>



Fractal::Fractal()
{

}

void Fractal::DrawKochSnowflake(QGraphicsView* view,
                         QGraphicsScene * scene,
                         int angles_count,
                         int iterations,
                         QPointF start_point,
                         float line_size,
                         float height)
{
    QMatrix4x4 rot_mat;
    QPointF last_point = start_point;
    rot_mat.setToIdentity();
    QVector3D unrotated(line_size, 0, 0);

    int angle = (360 / angles_count);

    for(int i = 0; i < angles_count; i++)
    {

        QVector3D rotated = rot_mat * unrotated;
        QVector3D next_point = QVector3D(last_point) + rotated;
        DrawKochCurve(view,
                      scene,
                      iterations,
                      1,
                      last_point,
                      QPointF(next_point.x(), next_point.y()),
                      height,
                      angles_count);

        last_point = QPointF(next_point.x(), next_point.y());
        rot_mat.rotate(angle, QVector3D(0, 0, 1));

    }
}

void Fractal::DrawKochCurve(QGraphicsView* view,
                            QGraphicsScene * scene,
                            int iterations,
                            int current_iteration,
                            QPointF start_point,
                            QPointF end_point,
                            float height,
                            int angles_count)
{
    QPointF *points = new QPointF[angles_count + 2];
    points[0] = start_point;
    points[angles_count + 1] = end_point;
    points[1].setX(start_point.x() + (end_point.x() - start_point.x()) / 3); // x0 + (x_last - x0) / 3
    points[1].setY(start_point.y() + (end_point.y() - start_point.y()) / 3);
    points[angles_count].setX(start_point.x() + (end_point.x() - start_point.x()) * 2 / 3); // x0 + (x_last - x0) * 2 / 3
    points[angles_count].setY(start_point.y() + (end_point.y() - start_point.y()) * 2 / 3);

    double lineSize = sqrt(pow(start_point.x() - end_point.x(),2) + pow(start_point.y() - end_point.y(),2));
    double cosAngle = (start_point.x() - end_point.x()) / lineSize;
    double sinAngle = (start_point.y() - end_point.y()) / lineSize;

    if (angles_count == 3) // Triangle
    {
        points[2].setX(- height * sinAngle + (start_point.x() + end_point.x()) / 2);
        points[2].setY(height * cosAngle + (start_point.y() + end_point.y()) / 2); // sinL = 33` / H; 33` = sinL * H;
    }
    else if (angles_count == 4) // Square
    {
        points[2].setX(- height * sinAngle + points[1].x());
        points[2].setY(height * cosAngle + points[1].y());
        points[3].setX(- height * sinAngle + points[4].x());
        points[3].setY(height * cosAngle + points[4].y());
    }

    if(current_iteration == iterations)
    {
        for(int i = 1; i < angles_count + 2; i++)
        {
            scene->addLine(QLineF(points[i-1], points[i]));
        }
    }
    else
    {
        for(int i = 1; i < angles_count + 2; i++)
        {
            DrawKochCurve(view,
                          scene,
                          iterations,
                          current_iteration + 1,
                          points[i-1],
                          points[i], height / 3,
                          angles_count);
        }
    }
    delete[] points;
}

void Fractal::DrawSierpinskiTriangle(QGraphicsView* view,
                                     QGraphicsScene * scene,
                                     int iterations,
                                     int current_iteration,
                                     QPoint p1,
                                     QPoint p2,
                                     QPoint p3)
{
    QBrush hole_brush;
    hole_brush.setColor(Qt::white);
    hole_brush.setStyle(Qt::SolidPattern);
    if(current_iteration == 1)
    {
        QPolygon polygon;

        polygon.append(p1);
        polygon.append(p2);
        polygon.append(p3);
        polygon.append(p1);
        QBrush brush;
        brush.setColor(Qt::black);
        brush.setStyle(Qt::SolidPattern);
        scene->addPolygon(polygon, QPen(), brush);
    }

    QPoint left_mid((p1 + p2) / 2);
    QPoint right_mid((p3 + p2) / 2);
    QPoint bot_mid((p1 + p3) / 2);

    if(current_iteration != iterations)
    {
        QPolygon hole;
        hole.append(left_mid);
        hole.append(right_mid);
        hole.append(bot_mid);
        hole.append(left_mid);
        scene->addPolygon(hole, QPen(), hole_brush);
        DrawSierpinskiTriangle(view, scene, iterations, current_iteration + 1, p1, left_mid, bot_mid);
        DrawSierpinskiTriangle(view, scene, iterations, current_iteration + 1, left_mid, p2, right_mid);
        DrawSierpinskiTriangle(view, scene, iterations, current_iteration + 1, bot_mid, right_mid, p3);
    }

}

void Fractal::DrawSierpinskiSquare(QGraphicsView* view,
                                 QGraphicsScene * scene,
                                 int iterations,
                                 int current_iteration,
                                 QPoint p1,
                                 QPoint p2,
                                 QPoint p3,
                                 QPoint p4)
{
    QBrush hole_brush;
    hole_brush.setColor(Qt::white);
    hole_brush.setStyle(Qt::SolidPattern);
    if(current_iteration == 1)
    {
        QPolygon polygon;
        polygon.append(p1);
        polygon.append(p2);
        polygon.append(p3);
        polygon.append(p4);
        polygon.append(p1);
        QBrush brush;
        brush.setColor(Qt::black);
        brush.setStyle(Qt::SolidPattern);
        scene->addPolygon(polygon, QPen(), brush);
    }

    QPoint o_l[2] = { p1 + (p2 - p1) / 3, p1 + (p2 - p1) * (2 / 3.) };
    QPoint o_t[2] = { p2 + (p3 - p2) / 3, p2 + (p3 - p2) * (2 / 3.) };
    QPoint o_r[2] = { p3 + (p4 - p3) / 3, p3 + (p4 - p3) * (2 / 3.) };
    QPoint o_b[2] = { p4 + (p1 - p4) / 3, p4 + (p1 - p4) * (2 / 3.) };
    QPoint i_b_l = o_l[0] + (o_r[1] - o_l[0]) / 3;
    QPoint i_b_r = o_l[0] + (o_r[1] - o_l[0]) * (2 / 3.);
    QPoint i_t_l = o_l[1] + (o_r[0] - o_l[1]) / 3;
    QPoint i_t_r = o_l[1] + (o_r[0] - o_l[1]) * (2 / 3.);

    std::vector<QPoint> all_points;

    if(current_iteration != iterations)
    {
        QPolygon hole;
        hole.append(i_b_l);
        hole.append(i_t_l);
        hole.append(i_t_r);
        hole.append(i_b_r);
        hole.append(i_b_l);
        scene->addPolygon(hole, QPen(), hole_brush);
        DrawSierpinskiSquare(view, scene, iterations, current_iteration + 1, p1, o_l[0], i_b_l, o_b[1]);
        DrawSierpinskiSquare(view, scene, iterations, current_iteration + 1, o_l[0], o_l[1], i_t_l, i_b_l);
        DrawSierpinskiSquare(view, scene, iterations, current_iteration + 1, o_l[1], p2, o_t[0], i_t_l);
        DrawSierpinskiSquare(view, scene, iterations, current_iteration + 1, i_t_l, o_t[0], o_t[1], i_t_r);
        DrawSierpinskiSquare(view, scene, iterations, current_iteration + 1, o_t[1], p3, o_r[0], i_t_r);
        DrawSierpinskiSquare(view, scene, iterations, current_iteration + 1, o_r[0], o_r[1], i_b_r, i_t_r);
        DrawSierpinskiSquare(view, scene, iterations, current_iteration + 1, o_r[1], p4, o_b[0], i_b_r);
        DrawSierpinskiSquare(view, scene, iterations, current_iteration + 1, o_b[0], o_b[1], i_b_l, i_b_r);

    }



}

void Fractal::DrawSierpinskiFractal(QGraphicsView* view,
                                  QGraphicsScene * scene,
                                  int iterations,
                                  int num_of_points)
{
    switch(num_of_points)
    {
    case 3:
    {
        QPoint p1(-200, 300);
        QPoint p2(0, 0);
        QPoint p3(200, 300);
        DrawSierpinskiTriangle(view, scene, iterations, 1, p1, p2, p3);
        break;
    }
    case 4:
    {
        QPoint p1(-200, -200);
        QPoint p2(-200, 200);
        QPoint p3(200, 200);
        QPoint p4(200, -200);
        DrawSierpinskiSquare(view, scene, iterations, 1, p1, p2, p3, p4);
        break;
    }
    default:
        assert(false);
    }
}

