#ifndef COLORMODEL_H
#define COLORMODEL_H

#define RANGE_TRANSFORM_VALUE 255.0
#define MAGENTA_HUE_VALUE 300
#define GREEN_HUE_VALUE 120
#define RANGE_SHIFT 20

#include <QImage>
#include <vector>
#include <cmath>
#include <QRgb>

class ColorModel
{
private:
    //vector<pixel_X, pixel_Y>
    static std::vector<std::pair<int, int>> pixels;

    static void applySaturationChange(QImage & image,
                                const int & saturation,
                                const std::pair<int, int> & pixel);

public:
    static void setPixels(const QImage & image);
    static void convertRgbToHsv(const QRgb & rgb, int & h, int & s, int & v);
    static void convertRgbToCMYK(const QRgb & rgb, int & c, int & m, int & y, int & k);
    static bool changeSaturation(QImage & image, QRect & rect, const int & newSaturation);
    static QRgb convertHsvToRgb(const int & h, const int & s, const int & v);
};

#endif // COLORMODEL_H
