QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ../ColorModelWindow/colormodelwindow.cpp \
    ../FractalsBuilderWindow/fractalsbuilderwindow.cpp \
    ../FractalsBuilderWindow/kochlinedialog.cpp \
    ../HelpWindows/colorhelpwindow.cpp \
    ../HelpWindows/fractalhelpwindow.cpp \
    ../HelpWindows/helpwindow.cpp \
    ../HelpWindows/trianglehelpwindow.cpp \
    ../TriangleWindow/trianglewindow.cpp \
    Fractal.cpp \
    colormodel.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    ../ColorModelWindow/colormodelwindow.h \
    ../FractalsBuilderWindow/fractalsbuilderwindow.h \
    ../FractalsBuilderWindow/kochlinedialog.h \
    ../HelpWindows/colorhelpwindow.h \
    ../HelpWindows/fractalhelpwindow.h \
    ../HelpWindows/helpwindow.h \
    ../HelpWindows/trianglehelpwindow.h \
    ../TriangleWindow/trianglewindow.h \
    Fractal.h \
    colormodel.h \
    mainwindow.h

FORMS += \
    ../ColorModelWindow/colormodelwindow.ui \
    ../FractalsBuilderWindow/fractalsbuilderwindow.ui \
    ../FractalsBuilderWindow/kochlinedialog.ui \
    ../HelpWindows/colorhelpwindow.ui \
    ../HelpWindows/fractalhelpwindow.ui \
    ../HelpWindows/helpwindow.ui \
    ../HelpWindows/trianglehelpwindow.ui \
    ../TriangleWindow/trianglewindow.ui \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc

DISTFILES += \
    mainWindowStyles.qss
