#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#define APPLICATION_HEADER "Computer graphics study app"
#include <QMainWindow>
#include <QDesktopServices>
#include <QUrl>
#include "../FractalsBuilderWindow/fractalsbuilderwindow.h"
#include "../ColorModelWindow/colormodelwindow.h"
#include "../TriangleWindow/trianglewindow.h"
#include "../HelpWindows/helpwindow.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

enum PREVIOUS_WINDOW_TYPE
{
    HELP_WINDOW,
    EDITOR_WINDOW
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_fractals_clicked();

    void on_pushButton_colorModel_clicked();

    void on_pushButton_exit_clicked();

    void on_pushButton_help_clicked();

    void on_pushButton_triangleMove_clicked();

private:
    Ui::MainWindow *ui;

    void initToolTips();
};
#endif // MAINWINDOW_H
