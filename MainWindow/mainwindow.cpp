#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle(QCoreApplication::applicationName());
    this->setFixedSize(this->width(),this->height());
    this->initToolTips();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_fractals_clicked()
{
    FractalsBuilderWindow *fractalsBuilder = new FractalsBuilderWindow;
    fractalsBuilder->show();
    close();
}

void MainWindow::on_pushButton_colorModel_clicked()
{
    ColorModelWindow *colorModel = new ColorModelWindow;
    colorModel->show();
    close();
}

void MainWindow::on_pushButton_help_clicked()
{
    HelpWindow *helpWindow = new HelpWindow;
    helpWindow->show();
    close();
}

void MainWindow::on_pushButton_exit_clicked()
{
      QApplication::quit();
}

void MainWindow::on_pushButton_triangleMove_clicked()
{
    TriangleWindow *triangleWindow = new TriangleWindow;
    triangleWindow->show();
    close();
}

void MainWindow::initToolTips()
{
    ui->pushButton_fractals->setToolTip("Study the process of building the fractal using the following examples:"
                                        "\n1. Build koch snowflake using triangle and rectangle shapes."
                                        "\n2. The Serpinski fractal using the triangle and rectangle shapes.");

    ui->pushButton_colorModel->setToolTip("Study the concept of color models conversions using the following examples:"
                                        "\n1. Study the color value of selected pixel in RGB, HSV and CMYK color models."
                                        "\n2. Change the saturation of purple and green colors.");

    ui->pushButton_triangleMove->setToolTip("Study the concept of affine transformations using the following examples:"
                                        "\n1. The shearing of the triangle."
                                        "\n2. The rotation of the triangle."
                                        "\n3. The reflection of the triangle.");

    ui->pushButton_help->setToolTip("Go to help window to find out more about the application itself");
}
