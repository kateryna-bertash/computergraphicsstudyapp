#include "colormodelwindow.h"
#include "ui_colormodelwindow.h"

ColorModelWindow::ColorModelWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ColorModelWindow)
{
    ui->setupUi(this);
    this->setWindowTitle(QCoreApplication::applicationName());
    this->setFixedSize(this->width(),this->height());
    this->initializeToolTips();
    scene = new QGraphicsScene();
    ui->graphicsView->setScene(scene);
    ui->graphicsView->viewport()->installEventFilter(this);
    ui->graphicsView->viewport()->setMouseTracking(true);
    ResetColorModelsValuesDisplay();
}

ColorModelWindow::~ColorModelWindow()
{
    delete ui;
}

void ColorModelWindow::on_pushButton_back_clicked()
{
    if(changesNotSavedDialog() == QMessageBox::Cancel)
        return;

    MainWindow *mainWindow = new MainWindow;
    mainWindow->show();
    close();
}

void ColorModelWindow::on_pushButton_helpWhite_clicked()
{
    if(changesNotSavedDialog() == QMessageBox::Cancel)
        return;

    ColorHelpWindow *colorHelpWindow = new ColorHelpWindow();
    colorHelpWindow->setPreviousPage(1);
    colorHelpWindow->show();
    close();
}

void ColorModelWindow::on_pushButton_open_clicked()
{
    ResetColorModelsValuesDisplay();

    QString fileName= QFileDialog::getOpenFileName(this, "Open image", QCoreApplication::applicationDirPath(), "PNG (*.png)" );
    QImage *image = new QImage(fileName);
    QPixmap pixmap = InitializePixmap(*image);

    originalImage = pixmap.toImage();
    currImage = originalImage;
    on_pushButton_reset_clicked();

    topLImage.setX((ui->graphicsView->width() - currImage.width())/2.0);
    topLImage.setY((ui->graphicsView->height() - currImage.height())/2.0);

    ColorModel::setPixels(currImage);

    DisplayImage(*image);
}

void ColorModelWindow::on_pushButton_original_clicked()
{
    if(ui->pushButton_original->text() == "Original"){
        on_pushButton_reset_clicked();
        DisplayImage(originalImage);
        ui->pushButton_original->setText("Current version");
    }else
    {
        DisplayImage(currImage);
        ui->pushButton_original->setText("Original");
    }
}

void ColorModelWindow::on_pushButton_save_clicked()
{
    QString fileName= QFileDialog::getSaveFileName(this, "Save image", QCoreApplication::applicationDirPath(), "PNG (*.png)" );

    if (!fileName.isNull())
    {
        //Copy qgraphicsView to not change scaling of the original graphicsView
        QGraphicsView *graphicsView = new QGraphicsView(ui->graphicsView->scene());
        graphicsView->setMinimumSize(ui->graphicsView->width(), ui->graphicsView->height());
        graphicsView->fitInView(graphicsView->scene()->sceneRect(), Qt::KeepAspectRatio );

        QImage image(graphicsView->scene()->sceneRect().size().toSize(), QImage::Format_ARGB32);
        image.fill(Qt::transparent);

        QPainter painter(&image);
        graphicsView->scene()->render(&painter);
        image.save(fileName);
    }
}

bool ColorModelWindow::eventFilter(QObject * obj, QEvent * event)
{
    if(!ui->graphicsView->scene() || ui->graphicsView->scene()->items().empty())
        return false;

    if (event->type() == QEvent::MouseMove)
    {
        //Capture all the mouse move events over the qgraphicsview
        //to display pixel's colors values

        QMouseEvent *mEvent = static_cast<QMouseEvent*>(event);
        QPoint currPoint(mEvent->x(), mEvent->y());
        ApplyImagePositionOnCanvas(currImage, currPoint);

        if(currPoint.x() != -1 && currPoint.y() != -1)
        {
            QRgb rgb = currImage.pixel(currPoint.x(), currPoint.y());

            int h = 0, s = 0, v = 0;
            ColorModel::convertRgbToHsv(rgb, h, s, v);

            int c = 0, m = 0, k= 0, y = 0;
            ColorModel::convertRgbToCMYK(rgb, c, m, y, k);

            DisplayRgb(rgb);
            DisplayHsv(h, s, v);
            DisplayCMYK(c, m, y, k);
        }
    }
    else if(event->type() == QEvent::MouseButtonPress)
    {
        //Get coords to start drawing selection rectangle
        QMouseEvent *mEvent = static_cast<QMouseEvent*>(event);
        topLRect = QPoint(mEvent->x(), mEvent->y());
    }
    else if(event->type() == QEvent::MouseButtonRelease)
    {
        //Get coords to start stop drawing selection rectangle
        QMouseEvent *mEvent = static_cast<QMouseEvent*>(event);
        int w = std::abs(mEvent->x() - topLRect.x());
        int h = std::abs(mEvent->y() - topLRect.y());
        rect = QRect(topLRect.x(), topLRect.y(), w, h);

        if(scene->items().size() > 1) //if there is already rect on the scene
        {
            scene->clear();
            DisplayImage(currImage);
        }

        DisplaySelectionRectangle(rect);
    }

    return QMainWindow::eventFilter(obj, event);
}

void ColorModelWindow::ApplyImagePositionOnCanvas(const QImage &image, QPoint & point)
{
    if(point.x() < topLImage.x() || point.x() > topLImage.x() + image.width()
       || point.y() < topLImage.y() || point.y() > topLImage.y() + image.height())
    {
        point.setX(-1);
        point.setY(-1);
        return;
    }

    point.setX(point.x() - topLImage.x());
    point.setY(point.y() - topLImage.y());

    if(point.x() >= image.width() || point.y() >= image.height())
    {
        point.setX(-1);
        point.setY(-1);
    }
}

void ColorModelWindow::DisplayRgb(const QRgb & rgb)
{
    ui->label_modelRvalue->setText(QString::number(qRed(rgb)));
    ui->label_modelGvalue->setText(QString::number(qGreen(rgb)));
    ui->label_modelBvalue->setText(QString::number(qBlue(rgb)));
}

void ColorModelWindow::DisplayHsv(const int &h, const int &s, const int &v)
{
    ui->label_modelHvalue->setText(QString::number(h));
    ui->horizontalSlider_modelH->setValue(h);
    ui->label_modelSvalue->setText(QString::number(s) + "%");
    ui->horizontalSlider_modelS->setValue(s);
    ui->label_modelVvalue->setText(QString::number(v) + "%");
    ui->horizontalSlider_modelV->setValue(v);
}

void ColorModelWindow::DisplayCMYK(const int &c, const int &m, const int &y, const int &k)
{
    ui->label_modelCvalue->setText(QString::number(c) + "%");
    ui->label_modelMvalue->setText(QString::number(m) + "%");
    ui->label_modelYvalue->setText(QString::number(y) + "%");
    ui->label_modelKvalue->setText(QString::number(k) + "%");
}

void ColorModelWindow::DisplayImage(const QImage &image)
{
    if(scene)
        scene->clear();

    QPixmap pixmap = InitializePixmap(image);

    QGraphicsPixmapItem *item = new QGraphicsPixmapItem(pixmap);
    item->setPos((ui->graphicsView->width() - pixmap.width())/2.0, 0);
    scene->addItem(item);
    DisplaySelectionRectangle(rect);
    ui->graphicsView->setScene(scene);
}

void ColorModelWindow::DisplaySelectionRectangle(QRect &rect)
{
    if(rect.height() == 0 && rect.width() == 0)
        return;

    QGraphicsRectItem *rectItem = new QGraphicsRectItem(rect);
    rectItem->setRect(QRect(topLRect.x(), topLRect.y(), rect.width(), rect.height()));
    rectItem->setBrush(QBrush(Qt::transparent));
    scene->addItem(rectItem);
}

QPixmap ColorModelWindow::InitializePixmap(const QImage &image)
{
    return QPixmap::fromImage(image.scaled(ui->graphicsView->width(),
                                           ui->graphicsView->height(),
                                           Qt::KeepAspectRatio));
}

void ColorModelWindow::ResetColorModelsValuesDisplay()
{
    ui->label_modelCvalue->setText("");
    ui->label_modelMvalue->setText("");
    ui->label_modelYvalue->setText("");
    ui->label_modelKvalue->setText("");
    ui->label_modelHvalue->setText("");
    ui->label_modelSvalue->setText("");
    ui->label_modelVvalue->setText("");
    ui->label_modelRvalue->setText("");
    ui->label_modelGvalue->setText("");
    ui->label_modelBvalue->setText("");
    ui->label_saturationValue->setText("");
    ui->horizontalSlider_saturation->setValue(0);
    ui->horizontalSlider_modelH->setValue(0);
    ui->horizontalSlider_modelS->setValue(0);
    ui->horizontalSlider_modelV->setValue(0);
}

void ColorModelWindow::on_horizontalSlider_saturation_sliderMoved(int position)
{
    if(!scene || scene->items().empty())
    {
        QMessageBox::information(this,
                                 "Information",
                                 "You need to open an image to change saturation of the coresponding pixels");
        return;
    }

    currImage = originalImage;

    //get selection rectangle coordinates on image
    QPoint currPoint(rect.topLeft().x(), rect.topLeft().y());
    ApplyImagePositionOnCanvas(currImage, currPoint);

    QRect scaledRect = QRect(currPoint.x(), currPoint.y(), rect.width(), rect.height());

    if(!ColorModel::changeSaturation(currImage, scaledRect, position))
    {
        QMessageBox::information(this,
                                 "Information",
                                 "Sorry, this image doesn't containt any magenta or green pixels.\n"
                                 "Try another one to explore change saturation function.");
    }
    else
    {
        DisplayImage(currImage);
        ui->label_saturationValue->setText(QString::number(position) + "%");
    }
}

QMessageBox::StandardButton ColorModelWindow::changesNotSavedDialog()
{
    QMessageBox::StandardButton reply = QMessageBox::warning(this,
                                                             "Warning",
                                                             "Do you really want to exit this window?\n"
                                                             "Changes are not saved",
                                                             QMessageBox::Save |
                                                             QMessageBox::Close|
                                                             QMessageBox::Cancel);

    if(reply == QMessageBox::Save)
        on_pushButton_save_clicked();

    return reply;
}

void ColorModelWindow::on_pushButton_reset_clicked()
{
    rect.setHeight(0);
    rect.setWidth(0);
    DisplayImage(currImage);
}

void ColorModelWindow::initializeToolTips()
{
    ui->pushButton_open->setToolTip("Open a new png image to study the process of conversion to CMYK and HSV color models");
    ui->pushButton_original->setToolTip("Switch between original and current version of edited image");
    ui->pushButton_reset->setToolTip("Reset selection rectangle to modify the full image");
    ui->pushButton_helpWhite->setToolTip("Go to color model help window");
    ui->pushButton_back->setToolTip("Go to the previous window");
    ui->pushButton_save->setToolTip("Save edited image on your pc");

    ui->label_modelC->setToolTip("Stands for Cyan pigment");
    ui->label_modelM->setToolTip("Stands for Magenta pigment");
    ui->label_modelY->setToolTip("Stands for Yellow pigment");
    ui->label_modelK->setToolTip("Stands for Black pigment");

    ui->label_modelCvalue->setToolTip("Value of Cyan pigment is in range from 0 to 100 persentages");
    ui->label_modelMvalue->setToolTip("Value of Magenta pigment is in range from 0 to 100 persentages");
    ui->label_modelYvalue->setToolTip("Value of Yellow pigment is in range from 0 to 100 persentages");
    ui->label_modelKvalue->setToolTip("Value of Black pigment is in range from 0 to 100 persentages");

    ui->label_modelR->setToolTip("Stands for Red pigment");
    ui->label_modelG->setToolTip("Stands for Green pigment");
    ui->label_modelB->setToolTip("Stands for Blue pigment");

    ui->label_modelRvalue->setToolTip("Value of Red pigment is in range from 0 to 255");
    ui->label_modelGvalue->setToolTip("Value of Green pigment is in range from 0 to 255");
    ui->label_modelBvalue->setToolTip("Value of Blue pigment is in range from 0 to 255");

    ui->label_modelH->setToolTip("Stands for Hue.\nIt is the color portion of the model");
    ui->label_modelS->setToolTip("Stands for Saturation.\nIt describes the amount of gray in a particular color");
    ui->label_modelV->setToolTip("Stands for Value.\nIt works in conjunction with saturation and describes the brightness or intensity of the color");

    ui->label_modelHvalue->setToolTip("The value of hue is expressed by a number from 0 to 360 degrees: "
                                      "\nRed falls between 0 and 60 degrees."
                                      "\nYellow falls between 61 and 120 degrees."
                                      "\nGreen falls between 121 and 180 degrees."
                                      "\nCyan falls between 181 and 240 degrees."
                                      "\nBlue falls between 241 and 300 degrees."
                                      "\nMagenta falls between 301 and 360 degrees.");

    ui->label_modelSvalue->setToolTip("Value of saturation is in range from 0 to 100 percent. "
                                      "\nReducing this component toward zero introduces more\ngray and produces a faded effect.");

    ui->label_modelVvalue->setToolTip("Value is in range from 0 to 100 percent,\nwhere 0 is completely black,\nand 100 is the brightest.");

    ui->horizontalSlider_saturation->setToolTip("Saturation describes the amount of gray in a particular color, from 0 to 100 percent. "
                                                "\nReducing this component toward zero introduces more gray and produces a faded effect.");

    ui->label_activePath->setToolTip("The current directory");
}
