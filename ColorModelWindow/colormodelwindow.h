#ifndef COLORMODELWINDOW_H
#define COLORMODELWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QMouseEvent>
#include "mainwindow.h"
#include "colormodel.h"

namespace Ui {
class ColorModelWindow;
}

class ColorModelWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ColorModelWindow(QWidget *parent = nullptr);
    ~ColorModelWindow();

private slots:
    void on_pushButton_back_clicked();

    void on_pushButton_helpWhite_clicked();

    void on_pushButton_open_clicked();

    void on_pushButton_original_clicked();

    void on_pushButton_save_clicked();

    void on_horizontalSlider_saturation_sliderMoved(int position);

    void on_pushButton_reset_clicked();

protected slots:
    virtual bool eventFilter(QObject *obj, QEvent *ev);

private:
    Ui::ColorModelWindow *ui;
    QImage currImage;
    QImage originalImage;
    QGraphicsScene * scene;
    QPoint topLRect;
    QPoint topLImage;
    QRect rect;

    void ApplyImagePositionOnCanvas(const QImage & image, QPoint & point);

    void DisplayRgb(const QRgb & rgb);

    void DisplayHsv(const int & h, const int & s, const int & v);

    void DisplayCMYK(const int & c, const int & m, const int & y, const int & k);

    void DisplayImage(const QImage &image);

    void DisplaySelectionRectangle(QRect & rect);

    QPixmap InitializePixmap(const QImage &image);

    void ResetColorModelsValuesDisplay();

    QMessageBox::StandardButton changesNotSavedDialog();

    void initializeToolTips();
};

#endif // COLORMODELWINDOW_H
